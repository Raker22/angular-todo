function addGlobal(key: string, value: any): void {
  window[key] = value;
}

function apply (source: Dictionary, dest: Dictionary): any {
  for (const key in source) {
    if (source.hasOwnProperty(key)) {
      dest[key] = source[key];
    }
  }

  return dest;
}

addGlobal('addGlobal', addGlobal);
addGlobal('apply', apply);
