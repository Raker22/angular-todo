export class Todo {
  static nextId: number = 0;
  static getNextId(): number {
    return Todo.nextId++;
  }

  id: number = Todo.getNextId();
  text: string;
  completed: boolean = false;

  constructor(parameters: TodoParams ) {
    apply(parameters, this);
  }
}

export interface TodoParams {
  id?: number;
  text: string;
  completed?: boolean;
}
