import {Component, ElementRef, ViewChild} from '@angular/core';
import {Todo} from 'app/models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: 'todoList.component.html',
  styleUrls: ['todoList.component.css']
})
export class TodoListComponent {
  @ViewChild('todoInput') todoInputRef: ElementRef;
  get todoInput(): HTMLInputElement {
    return this.todoInputRef.nativeElement;
  }

  todos: Todo[] = [];
  completedFilter: boolean | null = null;

  get filteredTodos(): Todo[] {
    let filteredTodos: Todo[];

    if (this.completedFilter == null) {
      filteredTodos = this.todos.slice();
    }
    else {
      filteredTodos = this.todos.filter((todo: Todo) => {
        return todo.completed === this.completedFilter;
      });
    }

    return filteredTodos;
  }

  get someComplete(): boolean {
    return this.todos.some((todo: Todo) => {
      return todo.completed;
    });
  }

  get allComplete(): boolean {
    return this.todos.every((todo: Todo) => {
      return todo.completed;
    });
  }

  get completeAllEnabled(): boolean {
    return this.completedFilter !== true && !this.allComplete;
  }

  get clearCompleteEnabled(): boolean {
    return this.completedFilter !== false && this.someComplete;
  }

  copyTodos(): void {
    this.todos = this.todos.slice();
  }

  addInputTodo(): void {
    this.addTodo(this.todoInput.value);
    this.todoInput.value = '';
  }

  addTodo(text: string): void {
    text = text.trim();

    if (text.length > 0) {
      this.todos.push(new Todo({ text: text }));
      this.copyTodos();
    }
  }

  removeTodo(todoToRemove: Todo): void {
    const index: number = this.todos.findIndex((todo: Todo) => {
      return todo === todoToRemove;
    });

    this.todos.splice(index, 1);
    this.copyTodos();
  }

  clearComplete(): void {
    this.todos = this.todos.filter((todo: Todo) => {
      return !todo.completed;
    });
  }

  completeAll(): void {
    for (const todo of this.todos) {
      todo.completed = true;
    }
  }

  showAll(): void {
    this.completedFilter = null;
  }

  showActive(): void {
    this.completedFilter = false;
  }

  showComplete(): void {
    this.completedFilter = true;
  }
}
