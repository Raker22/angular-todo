import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {TodoModule} from 'app/components/todo/todo.module';
import {TodoListComponent} from 'app/components/todoList/todoList.component';

@NgModule({
  declarations: [TodoListComponent],
  exports: [TodoListComponent],
  imports: [
    BrowserModule,
    FormsModule,

    TodoModule
  ]
})
export class TodoListModule { }
