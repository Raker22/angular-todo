import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Todo} from 'app/models/todo';

@Component({
  selector: 'app-todo',
  templateUrl: 'todo.component.html',
  styleUrls: ['todo.component.css']
})
export class TodoComponent implements OnInit {
  static numTodos: number = 0;
  static getNextCheckboxId(): string {
    return `todoCheckbox${TodoComponent.numTodos++}`;
  }

  @Input() todo: Todo;
  @Output() todoChange: EventEmitter<Todo> = new EventEmitter<Todo>();
  @Output() completedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  checkboxId: string;
  editing: boolean = false;
  newText: string = '';

  ngOnInit(): void {
    this.checkboxId = TodoComponent.getNextCheckboxId();
  }

  emitTodo(): void {
    this.todoChange.emit(this.todo);
  }

  setCompleted(completed: boolean): void {
    this.todo.completed = completed;
    this.completedChange.emit(completed);
    this.emitTodo();
  }

  startEdit(): void {
    this.newText = this.todo.text;
    this.editing = true;
  }

  cancelEdit(): void {
    this.editing = false;
  }

  finishEdit(): void {
    if (this.editing) {
      this.todo.text = this.newText;
      this.editing = false;
      this.emitTodo();
    }
  }
}
