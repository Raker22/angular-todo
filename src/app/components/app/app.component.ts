import {Component, ElementRef, ViewChild} from '@angular/core';
import {Todo} from 'app/models/todo';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {

}
