/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

interface Dictionary<T = any> { [key: string]: T; }
