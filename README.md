# AngularTodo

## Comparison
### Development Speed
* Both relatively fast
* Angular is slowed down by having to create and manage modules
* React is slowed down by unidirectional communication and managing state

For me React was slower and more difficult to develop due to the unidirectional communication.
There is a level of bidirectional communication via callbacks but it is much more difficult to deal with.
React compiles much faster but both recompile quickly when changes are made.

### Performance
* React loads slightly faster
* Scale of project was not large enough effectively test app performance

### Development Process
* React makes no assumptions about your project so you have to a find third party library or write the functionality that is missing
* Angular apps are a little larger out of the box but they come with extra functionality that most developers want for a web app
* The Angular team has many optional libraries so you can get a lot of the functionality you need with confidence since it's backed by the framework's creators
* React has developed some libraries but the community is more heavily relied on

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.
